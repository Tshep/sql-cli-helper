'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('phone', 'oldcol')
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('phone', 'oldcol', Sequelize.STRING)
  },
}
