'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('phone', 'oldcol', 'newcol')
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('phone', 'newcol', 'oldcol')
  },
}
